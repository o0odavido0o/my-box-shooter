﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAreaChange : MonoBehaviour {

	public float PeriodOfTime = 30.0f;
	private float flCurrentTime;

	public GameObject[] Spawners;
	private int iInActiveSpawner = 0;

	// Use this for initialization
	void Start () {
		flCurrentTime = PeriodOfTime;
	}

	// Update is called once per frame
	void Update () {

		if (GameManager.gm) {
			if (GameManager.gm.gameIsOver) {
				Spawners [iInActiveSpawner].SetActive (false);
				return;
			}
		}

		if (flCurrentTime > 0) {
			flCurrentTime -= Time.deltaTime;
		}else{

			int iActiveSpawner = Random.Range (0, Spawners.Length);
			while (iActiveSpawner == iInActiveSpawner) {
				iActiveSpawner = Random.Range (0, Spawners.Length);
			}

			Spawners[iActiveSpawner].SetActive(true);
			Spawners [iInActiveSpawner].SetActive(false);

			iInActiveSpawner = iActiveSpawner;
			flCurrentTime = PeriodOfTime;
		}
}

}
